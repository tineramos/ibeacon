//
//  Config.h
//  iBeacons Hot and Cold
//
//  Created by Christine Ramos on 1/7/14.
//  Copyright (c) 2014 MobileTuts. All rights reserved.
//

#ifndef iBeacons_Hot_and_Cold_Config_h
#define iBeacons_Hot_and_Cold_Config_h

#define STRATPOINT_UUID         @"A5F477BA-F75E-4372-BCD0-D9F0317961D5"
#define STRATPOINT_IDENTIFIER   @"com.stratpoint.iBeacons"

#endif
