//
//  GetViewController.h
//  iBeacons Hot and Cold
//
//  Created by Jorge Costa on 10/18/13.
//  Copyright (c) 2013 MobileTuts. All rights reserved.
//

#import "ViewController.h"

#import <CoreLocation/CoreLocation.h>

@interface GetViewController : ViewController <CLLocationManagerDelegate>

@property (nonatomic, weak) IBOutlet UILabel *UUID;
@property (nonatomic, weak) IBOutlet UILabel *majorLabel;
@property (nonatomic, weak) IBOutlet UILabel *minorLabel;
@property (nonatomic, weak) IBOutlet UILabel *accuracyLabel;
@property (nonatomic, weak) IBOutlet UILabel *rssiLabel;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIImageView *introImageView;

@property (nonatomic, weak) IBOutlet UILabel *distanceLabel;

@property (nonatomic, strong) CLBeaconRegion *beaconRegion;
@property (nonatomic, strong) CLLocationManager *locationManager;

@end
