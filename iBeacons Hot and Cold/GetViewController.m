//
//  GetViewController.m
//  iBeacons Hot and Cold
//
//  Created by Jorge Costa on 10/18/13.
//  Copyright (c) 2013 MobileTuts. All rights reserved.
//

#import "GetViewController.h"

#import "Config.h"

@interface GetViewController ()

@end

@implementation GetViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self initRegion];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager startMonitoringForRegion:self.beaconRegion];
}

- (void)initRegion
{
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:STRATPOINT_UUID];
    self.beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:STRATPOINT_IDENTIFIER];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// tells the delegate that the user entered the specified region
// starts to deliver notifications for beacons in the specified region
- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    if (![self.beaconRegion isEqual:region]) {
        return;
    }
    
    // TODO: ignore event
    
    NSLog(@"BEACON found!");
    [self.locationManager startRangingBeaconsInRegion:self.beaconRegion];
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.soundName = UILocalNotificationDefaultSoundName;
    notification.alertBody = @"Welcome to Stratpoint Pantry! Get some beer in the fridge. Enjoy! :)";
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
}

// tells the delegate that the user left the specified region
// stops the delivery of notifications for beacons in the specified region
- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    NSLog(@"BEACON left!");
    [self.locationManager stopMonitoringForRegion:self.beaconRegion];
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.soundName = UILocalNotificationDefaultSoundName;
    notification.alertBody = @"You're full? Come again for snacks. :D";
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
}

// tells the delegate that one or more beacons are in range
- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
    CLBeacon *qualifyingBeacon = [self closestBeaconWithProximity:CLProximityNear fromBeacons:beacons];
    
    if (qualifyingBeacon) {
        [self showPresentation:qualifyingBeacon];
    }
    else {
        // dismiss presentation
        [self dismissPresentation];
    }
    
    if (qualifyingBeacon.proximity == CLProximityUnknown) {
        _distanceLabel.text = @"Unknown Proximity";
    } else if (qualifyingBeacon.proximity == CLProximityImmediate) {
        _distanceLabel.text = @"Immediate";
    } else if (qualifyingBeacon.proximity == CLProximityNear) {
        _distanceLabel.text = @"Near";
    } else if (qualifyingBeacon.proximity == CLProximityFar) {
        _distanceLabel.text = @"Far";
    }
}

// one-sigma horizontal accuracy - 39.4%
// source: http://gpsinformation.net/main/errors.htm
- (void)showPresentation:(CLBeacon *)beacon
{
    self.UUID.text = beacon.proximityUUID.UUIDString;
    self.majorLabel.text = [beacon.major stringValue];
    self.minorLabel.text = [beacon.minor stringValue];
    self.accuracyLabel.text = [NSString stringWithFormat:@"%f", beacon.accuracy];
    self.rssiLabel.text = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:beacon.rssi]];
    
    NSString *imageName = [NSString stringWithFormat:@"%@-%@", [beacon.major stringValue], [beacon.minor stringValue]];
    self.introImageView.image = [UIImage imageNamed:imageName];
    
    if ([beacon.major isEqualToNumber:[NSNumber numberWithInt:2]]) {
        self.titleLabel.text = @"The Pantry";
    }
    else {
        self.titleLabel.text = @"The Conference Room";
    }
}

- (void)dismissPresentation
{
    self.UUID.text = @"";
    self.majorLabel.text = @"";
    self.minorLabel.text = @"";
    self.accuracyLabel.text = @"";
    self.rssiLabel.text = @"";
    self.titleLabel.text = @"";
    self.introImageView.image = nil;
}

- (void)shouldIgnoreEvent
{
    
}

- (CLBeacon *)closestBeaconWithProximity:(CLProximity)proximity fromBeacons:(NSArray *)beacons
{
    for (CLBeacon *beacon in beacons) {
        if (beacon.proximity == proximity) {
            return beacon;
        }
    }
    return nil;
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
    [self.locationManager startRangingBeaconsInRegion:self.beaconRegion];
}

@end
