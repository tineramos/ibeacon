//
//  SendViewController.h
//  iBeacons Hot and Cold
//
//  Created by Jorge Costa on 10/18/13.
//  Copyright (c) 2013 MobileTuts. All rights reserved.
//

#import "ViewController.h"

#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface SendViewController : ViewController <CBPeripheralManagerDelegate>

@property (nonatomic, weak) IBOutlet UILabel *uuidLabel;
@property (nonatomic, weak) IBOutlet UILabel *majorLabel;
@property (nonatomic, weak) IBOutlet UILabel *minorLabel;
@property (nonatomic, weak) IBOutlet UILabel *identityLabel;

@property (nonatomic, weak) IBOutlet UIButton *transmitButton;

// used to define the settings (proximityUUID, major and minor) that are needed to set up a beacon as a transmitter
@property (nonatomic, strong) CLBeaconRegion *beaconRegion;

// property where the methods are contained to start it transmitting
@property (nonatomic, strong) CBPeripheralManager *peripheralManager;

// contains the peripheral data of the beacon
@property (nonatomic, strong) NSDictionary *beaconPeripheralData;

@end
