//
//  SendViewController.m
//  iBeacons Hot and Cold
//
//  Created by Jorge Costa on 10/18/13.
//  Copyright (c) 2013 MobileTuts. All rights reserved.
//

#import "SendViewController.h"

#import "Config.h"

@interface SendViewController ()

@end

@implementation SendViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    [self initBeacon];
}

- (void)initBeacon
{
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:STRATPOINT_UUID];
    
    self.beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid major:1 minor:2 identifier:STRATPOINT_IDENTIFIER];
    
    self.uuidLabel.text = _beaconRegion.proximityUUID.UUIDString;
    self.majorLabel.text = [_beaconRegion.major stringValue];
    self.minorLabel.text = [_beaconRegion.minor stringValue];
    self.identityLabel.text = _beaconRegion.identifier;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)transmit:(id)sender
{
    // method that retrieves data that can be used to advertise the current device as a beacon.
    self.beaconPeripheralData = [self.beaconRegion peripheralDataWithMeasuredPower:nil];

    // initialized with the delegate to receive the peripheral role events
    // the dispatch queue for dispatching the peripheral role events
    // optional dictionary containing initialization options for a peripheral manager
    self.peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil options:nil];    
}

#pragma mark - CBPeripheralManagerDelegate methods

// used to check the device state, and taking into consideration the state, the app acts accordingly.
// CBPeripheralManagerStatePoweredOn - app will begin to advertise
- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral
{
    if (peripheral.state == CBPeripheralManagerStatePoweredOn) {
        NSLog(@"Starting Advertisement!");
        [self.peripheralManager startAdvertising:self.beaconPeripheralData];
    } else if(peripheral.state == CBPeripheralManagerStatePoweredOff) {
        NSLog(@"Stoping Advertisement!");
        [self.peripheralManager stopAdvertising];
    }
}

@end
